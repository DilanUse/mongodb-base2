@extends('layouts.app')

@section('title-tab', 'Reporte de Ventas')
@section('title-page', 'Reporte de Ventas')

@section('content')
    <sales-report-component title='Reporte de Ventas' url-get="{{ route('sales.list') }}" gender-letter="o"></sales-report-component>
@endsection
