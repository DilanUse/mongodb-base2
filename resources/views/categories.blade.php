@extends('layouts.app')

@section('title-tab', 'Categorias')
@section('title-page', 'Categorias')

@section('content')
    <categories-component title='Categoria' url-get="{{ route('categories.list') }}" url-create="{{ route('categories.store') }}" url-delete="{{ route('categories.destroy', '') }}" url-update="{{ route('categories.update', '') }}" gender-letter="a"></categories-component>
@endsection
