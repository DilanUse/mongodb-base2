@extends('layouts.app')

@section('title-tab', 'Productos')
@section('title-page', 'Productos')

@section('content')
    <products-component title='Producto' url-get="{{ route('productos.listar') }}" url-create="{{ route('productos.store') }}" url-delete='productos/' url-update="productos/" gender-letter="o"></products-component>
@endsection
