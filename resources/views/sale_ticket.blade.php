<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ticket de caja</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div class="container col-3">
        <h3 class="text-center">MojiDogs</h3>
        <p class="text-center"><strong>Numero: {{ $sale->numero }}</strong></p>
        <h5>Datos del cliente:</h5>
        <p class="mb-0">DNI: {{ $sale->cliente['dni']}} </p>
        <p class="mb-0">Nombres: {{ $sale->cliente['first_name']}} </p>
        <p class="mb-0">Apellidos: {{ $sale->cliente['last_name']}} </p>
        <p class="mb-0">Telefono: {{ $sale->cliente['phone']}} </p>
        <p class="mb-0">Dirección: {{ $sale->cliente['address']}} </p>
        <hr>
        <h5 class="mt-3">Detalles:</h5>

        <table class="table table-borderless table-sm mt-3">
            <thead>
            <tr>
                <th scope="col" class="text-center" width="15%">UND.</th>
                <th scope="col" class="text-center" width="45%">PRODUCTO</th>
                <th scope="col" class="text-center" width="20%">PRECIO</th>
                <th scope="col" class="text-center" width="20%">SUBTOTAL</th>
            </tr>
            </thead>
            <tbody>
                @foreach($sale->detalles as $detail)

                        <tr >
                            <th scope="row" class="text-center">{{ $detail['amount'] }}</th>
                            <td class="text-center">{{ $detail['product']['nombre'] }}</td>
                            <td class="text-center">{{ $detail['product']['precio'] }}</td>
                            <td class="text-center">{{ $detail['subtotal'] }}</td>
                        </tr>
                @endforeach
            </tbody>
        </table>
        <hr>
        <div class="row">
            <div class="col-6">
                <h3>TOTAL</h3>
            </div>
            <div class="col-6">
                <h3 class="text-right">{{ $sale->total }}</h3>
            </div>
        </div>
        <hr>
        <button class="btn btn-block btn-secondary" onclick="window.print()">Imprimir</button>
    </div>
</body>
</html>
