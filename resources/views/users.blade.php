@extends('layouts.app')

@section('title-tab', 'Usuarios')
@section('title-page', 'Usuarios')

@section('content')
    <users-component title='Usuario' url-get="{{ route('users.list') }}" url-create="{{ route('users.store') }}" url-delete="{{ route('users.destroy', '') }}" url-update="{{ route('users.update', '') }}" gender-letter="o"></users-component>
@endsection
