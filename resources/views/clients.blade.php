@extends('layouts.app')

@section('title-tab', 'Clientes')
@section('title-page', 'Clientes')

@section('content')
    <clients-component title='Cliente' url-get="{{ route('clients.list') }}" url-create="{{ route('clients.store') }}" url-delete="{{ route('clients.destroy','') }}" url-update="{{ route('clients.update','') }}" gender-letter="o"></clients-component>
@endsection
