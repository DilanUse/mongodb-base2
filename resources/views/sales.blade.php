@extends('layouts.app')

@section('title-tab', 'Ventas')
@section('title-page', 'Generar Venta')

@section('content')
<sales-component title='Generar venta' numero="{{ $numero }}" url-cliente="{{ route('sales.getCliente') }}" url-create="{{ route('sales.store') }}" ticket-url="{{ route('sales.ticket', '') }}" gender-letter="a"></sales-component>
@endsection
