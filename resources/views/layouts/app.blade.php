<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Moji Dogs - @yield('title-tab')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    @auth
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('home') }}">Moji Dogs</a>
            <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <form action="{{ route('logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button class="btn btn-link nav-link">Salir</button>
                    </form>
                </li>
            </ul>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ $navbar == 'home' ? "active" : "" }}" href="{{ route('home') }}">
                                    <span data-feather="home"></span>
                                    Inicio - {{ auth()->user()->name }} <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            @if(auth()->user()->is_admin)
                                <li class="nav-item">
                                    <a class="nav-link {{ $navbar == 'categories' ? "active" : "" }}" href="{{ route('categories.index') }}">
                                        <span data-feather="file"></span>
                                        Categorias
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ $navbar == 'products' ? "active" : "" }}" href="{{ route('productos.index') }}">
                                        <span data-feather="file"></span>
                                        Productos
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ $navbar == 'users' ? "active" : "" }}" href="{{ route('users.index') }}">
                                        <span data-feather="users"></span>
                                        Usuarios
                                    </a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link {{ $navbar == 'clients' ? "active" : "" }}" href="{{ route('clients.index') }}">
                                    <span data-feather="users"></span>
                                    Clientes
                                </a>
                            </li>
                            <li class="nav-item">
                                <div class="accordion" id="collapseVentas">
                                    <a class="nav-link {{ $navbar == 'sales.report' || $navbar == 'sales' ? "active" : "" }}" data-toggle="collapse" href="#" data-target="#collapseOpciones" aria-expanded="false" aria-controls="collapseOpciones">
                                        <span data-feather="reporte-ventas"></span>
                                        Ventas
                                    </a>
                                    <div id="collapseOpciones" class="collapse ml-3 {{ $navbar == 'sales.report' || $navbar == 'sales' ? "show" : "" }}" aria-labelledby="opciones" data-parent="#collapseVentas">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link {{ $navbar == 'sales.report' ? "active" : "" }}" href="{{ route('sales.report') }}">
                                                    <span data-feather="bar-chart-2"></span>
                                                    Reporte de ventas
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link {{ $navbar == 'sales' ? "active" : "" }}" href="{{ route('sales.index') }}">
                                                    <span data-feather="bar-chart-2"></span>
                                                    Generar venta
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <main role="main" id="app" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 class="h2">@yield('title-page')</h1>
                    </div>
                    @yield('content')
                </main>
            </div>
        </div>
    @endauth

    @guest
        @yield('content')
    @endguest

    <!-- Scripts -->
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
