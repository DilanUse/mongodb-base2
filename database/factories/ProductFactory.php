<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Producto::class, function (Faker $faker) {
    $category = Category::offset(random_int(0, Category::count() - 1))->first();


    return [
        "nombre" => $faker->word,
        'descripcion' => $faker->text,
        'precio' => $faker->randomFloat(2, 100, 100000),
        'costo' => $faker->randomFloat(2,100, 100000),
        'in_menu' => true,
        'tipo' => json_decode(json_encode($category)),
        'tipo_id' => $category->_id,
    ];
});
