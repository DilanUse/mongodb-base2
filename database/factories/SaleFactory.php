<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Model;
use App\Producto;
use App\Sale;
use Faker\Generator as Faker;

$factory->define(Sale::class, function (Faker $faker) {
    $client = Client::offset(random_int(0, Client::count() - 1))->first();
//    $number = random_int();

    $detalles = [];

    for ($i = 0; $i < random_int(1, 10); $i++) {
        $product = Producto::offset(random_int(0, Producto::count() - 1))->first();
        $detalle = new stdClass();
        $detalle->product = $product;
        $detalle->amount = random_int(1, 5);
        $detalle->subtotal = $faker->randomFloat(2, 100, 10000);
        $detalle->price = $product->precio;
        $detalles[] = $detalle;
    }

    $days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    return [
        'numero' => $faker->unique()->numberBetween(),
        'dia' => $days[random_int(0, 6)],
        'mes' => $months[random_int(0, 11)],
        'total' => $faker->randomFloat(2, 1000, 100000),
        'detalles' => json_decode(json_encode($detalles)),
        'cliente' => json_decode(json_encode($client)),
    ];
});
