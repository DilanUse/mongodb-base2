<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'dni' => $faker->unique()->buildingNumber,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'sex' => random_int(0, 1) == 0 ? 'M' : 'F',
        'phone' => $faker->unique()->phoneNumber,
        'address' => $faker->address,
    ];
});
