<?php

use app\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'username' => 'administrador',
            'password' => bcrypt('administrador1234'),
            'is_admin' => True,
        ]);
        DB::table('users')->insert([
            'name' => 'Tori Perez',
            'username' => 'tperez',
            'password' => bcrypt('tperez1234'),
            'is_admin' => False,
        ]);
    }
}
