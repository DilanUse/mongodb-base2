<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
    if (Auth::check()) {
        return redirect('home');
    }
    else {
        return redirect('login');
    }

})->name('index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'Auth\LoginController@showLogin')->name('login');
Route::post('/login', 'Auth\LoginController@postLogin')->name('login');
Route::post('/logout', 'Auth\LoginController@logoutLogin')->name('logout');

Route::get('/productos/listar','ProductoController@list')->name('productos.listar');
Route::resource('/productos', 'ProductoController')->except([
    'create', 'edit', 'show'
]);

Route::get('/categories/list','CategoryController@list')->name('categories.list');
Route::resource('/categories', 'CategoryController')->except([
    'create', 'edit', 'show'
]);

Route::get('/clients/list','ClientController@list')->name('clients.list');
Route::resource('/clients', 'ClientController')->except([
    'create', 'edit', 'show'
]);

Route::get('/users/list','UserController@list')->name('users.list');
Route::resource('/users', 'UserController')->except([
    'create', 'edit', 'show'
]);

Route::get('/sales/ticket/{sale}','SaleController@ticket')->name('sales.ticket');
Route::get('/sales/list','SaleController@list')->name('sales.list');
Route::get('/sales/report','SaleController@report')->name('sales.report');
Route::get('/sales/getCliente','SaleController@getCliente')->name('sales.getCliente');
Route::resource('/sales', 'SaleController')->except([
    'create', 'edit', 'show'
]);

Route::get('/report/salesPerDay','ReportController@salesPerDay')->name('report.salesPerDay');
Route::get('/report/salesPerMonth','ReportController@salesPerMonth')->name('report.salesPerMonth');
Route::get('/report/clientsPerSex','ReportController@clientsPerSex')->name('report.clientsPerSex');

