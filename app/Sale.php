<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Sale extends MongoModel
{
    protected $table = 'sales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id','numero', 'total', 'dia', 'detalles', 'cliente'
    ];
}
