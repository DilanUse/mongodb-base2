<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;


class Category extends MongoModel
{
    protected $fillable = ['name'];

    // Query Scope
    public function scopeName($query, $name)
    {
        if($name)
        {
            return $query->where('name', 'LIKE', "%$name%");
        }
    }
}
