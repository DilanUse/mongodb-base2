<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Client extends MongoModel
{
    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id','dni', 'first_name', 'last_name', 'sex', 'phone', 'address'
    ];
}
