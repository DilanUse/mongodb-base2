<?php

namespace App\Http\Controllers;

use App\Client;
use App\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function salesPerDay()
    {
        $mon = Sale::where('dia', 'Mon')->sum('total');
        $tue = Sale::where('dia', 'Tue')->sum('total');
        $wed = Sale::where('dia', 'Wed')->sum('total');
        $thu = Sale::where('dia', 'Thu')->sum('total');
        $fri = Sale::where('dia', 'Fri')->sum('total');
        $sat = Sale::where('dia', 'Sat')->sum('total');
        $sun = Sale::where('dia', 'Sun')->sum('total');

        return [
            $mon,
            $tue,
            $wed,
            $thu,
            $fri,
            $sat,
            $sun,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function salesPerMonth()
    {
        return [
            Sale::where('mes', 1)->sum('total'),
            Sale::where('mes', 2)->sum('total'),
            Sale::where('mes', 3)->sum('total'),
            Sale::where('mes', 4)->sum('total'),
            Sale::where('mes', 5)->sum('total'),
            Sale::where('mes', 6)->sum('total'),
            Sale::where('mes', 7)->sum('total'),
            Sale::where('mes', 8)->sum('total'),
            Sale::where('mes', 9)->sum('total'),
            Sale::where('mes', 10)->sum('total'),
            Sale::where('mes', 11)->sum('total'),
            Sale::where('mes', 12)->sum('total'),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clientsPerSex()
    {
        return [
            Client::where('sex', 'M')->count(),
            Client::where('sex', 'F')->count(),
        ];
    }
}
