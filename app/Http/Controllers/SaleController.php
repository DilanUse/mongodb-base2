<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Client;
use Illuminate\Http\Request;
use DateTime;
use DateInterval;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $navbar = 'sales';
        $numero = Sale::max('numero');
        $numero = $numero?$numero:0;
        $numero = $numero + 1;
        return view('sales', compact('navbar','numero'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ticket(Request $request, Sale $sale)
    {
        return view('sale_ticket', compact('sale'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report()
    {
        $navbar = 'sales.report';
        return view('sales_report', compact('navbar'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $numero = Sale::max('numero');
        $numero = $numero ? $numero + 1 : 0;

        $sale = Sale::create([
            'numero' => $numero,
            'dia' => date('D'),
            'total' => $request['total'],
            'detalles' => $request['detalles'],
            'cliente' => $request['cliente'],
        ]);

        return $sale->_id;
    }

    /**
     * Return info about a client
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $dni
     * @return \Illuminate\Http\Response
     */
    public function getCliente(Request $request){
        $dni = $request->get('dni');
        $cliente = Client::where('dni',$dni)->first();
        return $cliente;
    }

     /**
     * Display the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $search = $request->get('search');
        $inicio = '';
        $fin = '';
        if($request->get('inicio')){
            $inicio = new DateTime($request->get('inicio'));
        }
        if($request->get('fin')){
            $fin = new DateTime($request->get('fin'));
            $fin->add(new DateInterval('P1D'));
        }

        $sales = Sale::when($inicio, function ($query, $inicio) {
                            return $query->where('created_at', '>=', $inicio);
                })
                ->when($fin, function ($query, $fin) {
                    return $query->where('created_at', '<=', $fin);
                })                
                ->orderBy('created_at', 'DESC')
                ->when($search, function ($query, $search) {
                    return $query->where('nombre', 'LIKE', "%$search%")
                                ->orWhere('descripcion', 'LIKE', "%$search%");
                })
                ->paginate(10);

        return [
            'pagination' => [
                'total'         => $sales->total(),
                'current_page'  => $sales->currentPage(),
                'per_page'      => $sales->perPage(),
                'last_page'     => $sales->lastPage(),
                'from'          => $sales->firstItem(),
                'to'            => $sales->lastItem(),
            ],
            'items' => $sales
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
