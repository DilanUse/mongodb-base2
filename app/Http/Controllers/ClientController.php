<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $navbar = 'clients';
        return view('clients', compact('navbar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Client::create([
            'dni' => $request['dni'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'sex' => $request['sex'],
            'phone' => $request['phone'],
            'address' => $request['address'],
        ]);
    }

/**
     * Display the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $search = $request->get('search');

        $clients = Client::orderBy('first_name', 'ASC')
                    ->when($search, function ($query, $search) {
                        return $query->where('dni', 'LIKE', "%$search%")
                            ->orWhere('first_name', 'LIKE', "%$search%")
                            ->orWhere('last_name', 'LIKE', "%$search%");
                    })
                    ->paginate(10);

        return [
            'pagination' => [
                'total'         => $clients->total(),
                'current_page'  => $clients->currentPage(),
                'per_page'      => $clients->perPage(),
                'last_page'     => $clients->lastPage(),
                'from'          => $clients->firstItem(),
                'to'            => $clients->lastItem(),
            ],
            'items' => $clients
        ];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Client::where('_id', $id)
        ->update([
            'dni' => $request['dni'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'sex' => $request['sex'],
            'phone' => $request['phone'],
            'address' => $request['address'],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::where('_id', $id)->delete();
    }
}
