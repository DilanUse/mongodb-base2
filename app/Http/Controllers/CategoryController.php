<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $navbar = 'categories';
        return view('categories', compact('navbar'));
    }

    /**
     * Display the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $all = $request->get('all');

        if ($all) {
            $categories = Category::orderBy('created_at', 'DESC')
                ->name($request->get('name'))
                ->get();
        }
        else {
            $categories = Category::orderBy('created_at', 'DESC')
                ->name($request->get('name'))
                ->paginate(10);
        }


        return [
            'pagination' => [
                'total'         => $categories->total(),
                'current_page'  => $categories->currentPage(),
                'per_page'      => $categories->perPage(),
                'last_page'     => $categories->lastPage(),
                'from'          => $categories->firstItem(),
                'to'            => $categories->lastItem(),
            ],
            'items' => $categories
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => ['required', 'unique:categories,name'],
        ]);

        Category::create($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $data = $this->validate($request, [
            'name' => ['required',  Rule::unique('categories')->ignore($category->_id)],
        ]);

        $category->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
    }
}
