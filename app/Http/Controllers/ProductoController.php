<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $navbar = 'products';
        return view('products', compact('navbar'));
    }

    /**
     * Display the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $search = $request->get('search');
        $tipo = $request->get('tipo');
        $inMenu = $request->get('inMenu');

        $productos = Producto::orderBy('created_at', 'DESC')
            ->when($search, function ($query, $search) {
                return $query->where('nombre', 'LIKE', "%$search%")
                    ->orWhere('descripcion', 'LIKE', "%$search%");
            })
            ->when($tipo, function ($query, $tipo) {
                return $query->where('tipo_id', '=', $tipo);
            })
            ->when($inMenu, function ($query, $inMenu) {
                return $query->where('in_menu', '=', true);
            })
            ->paginate(10);

        return [
            'pagination' => [
                'total'         => $productos->total(),
                'current_page'  => $productos->currentPage(),
                'per_page'      => $productos->perPage(),
                'last_page'     => $productos->lastPage(),
                'from'          => $productos->firstItem(),
                'to'            => $productos->lastItem(),
            ],
            'items' => $productos
        ];
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $data)
    {
        Producto::create([
            'nombre' => $data['nombre'],
            'descripcion' => $data['descripcion'],
            'precio' => $data['precio'],
            'costo' => $data['costo'],
            'in_menu' => $data['in_menu'],
            'tipo' => $data['tipo'],
            'tipo_id' => $data['tipo_id'],
        ]);

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $data, $id)
    {
        Producto::where('_id', $id)
        ->update([
            'nombre' => $data['nombre'],
            'descripcion' => $data['descripcion'],
            'precio' => $data['precio'],
            'costo' => $data['costo'],
            'in_menu' => $data['in_menu'],
            'tipo' => $data['tipo'],
            'tipo_id' => $data['tipo_id'],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Producto::where('_id', $id)->delete();
    }
}
