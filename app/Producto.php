<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Producto extends MongoModel
{
    protected $table = 'productos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id','nombre', 'descripcion', 'precio', 'costo', 'in_menu', 'tipo', 'tipo_id'
    ];
}
